#!/bin/bash

# shell scipt to prepend i3status with more stuff

i3status --config ~/.config/i3status/config | while :
do
        read -r line
        LG=$(xkb-switch) 
        echo "$LG | $line" || exit 1
done
