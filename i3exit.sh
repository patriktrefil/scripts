#!/bin/sh

x=$(printf "No\nYes" | dmenu -i -p "Want to log out?")

case $x in
  Yes) i3-msg exit;;
esac
